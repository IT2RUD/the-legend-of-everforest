﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour {
    Rigidbody2D FireBody;
	// Use this for initialization
    void LateUpdate()
    {
        ParticleSystem.Particle[] p = new ParticleSystem.Particle[GetComponent<ParticleSystem>().particleCount + 1];
        int l = GetComponent<ParticleSystem>().GetParticles(p);
        int i = 0;
        while (i < l)
        {
            p[i].velocity = -(FireBody.velocity.normalized) * 7;
            i++;
        }
        GetComponent<ParticleSystem>().SetParticles(p, l);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        FireBody = GetComponent<Rigidbody2D>();
        if (other.tag == "PlayerHitBox")
        {
            GameObject PlayerHit = other.transform.parent.gameObject;
            if (PlayerHit.GetComponent<Movement>().Disabled == false) {
                PlayerHit.GetComponent<Movement>().DisabledAmount = 40;
                PlayerHit.GetComponent<Movement>().Disabled = true;
                if (PlayerHit.GetComponent<Health>().PlayerHealth > 0)
                {
                    PlayerHit.GetComponent<Health>().PlayerHealth -= 10;
                }
                PlayerHit.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
                PlayerHit.GetComponent<Rigidbody2D>().AddForce(new Vector2(FireBody.velocity.x, Mathf.Abs(FireBody.velocity.x)).normalized * 300f);
            }
            Destroy(gameObject);
        }
    }
}
