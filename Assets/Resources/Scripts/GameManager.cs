﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public GameObject Player;
    CircleCollider2D PlayerColl;
    GameObject ColliderObject;
    static GameObject DamageUI;
    static GameObject Canvas;

    float Pause = 3;
    float PauseTimer;

    public static AudioSource Music, Parry, DashAttack, Attack1, Attack2, Attack3, Swing1, Swing2, Ground, JumpDamage, FireHit, Eat;

    public static List<AudioSource> Playing = new List<AudioSource>();
    public static List<float> PlayAmount = new List<float>();

    public static List<GameObject> DamageUIs = new List<GameObject>();
    // Use this for initialization
    void Start()
    {
        #region CreateTriggerColliders
        ColliderObject = new GameObject();
        BoxCollider2D OtherColl = ColliderObject.GetComponent<BoxCollider2D>();
        ColliderObject.name = "Collider";
        ColliderObject.transform.position = Player.transform.position;
        ColliderObject.transform.parent = Player.transform;
        ColliderObject.AddComponent<BoxCollider2D>();
        OtherColl = ColliderObject.GetComponent<BoxCollider2D>();
        PlayerColl = Player.GetComponent<CircleCollider2D>();
        OtherColl.offset = new Vector2(PlayerColl.offset.x, PlayerColl.offset.y - 0.2f);
        OtherColl.size = new Vector2(PlayerColl.radius * 2 - 0.1f, PlayerColl.radius * 2);
        OtherColl.isTrigger = true;
        ColliderObject.AddComponent<FloorDetector>();
        #endregion
        AudioSource[] AllAudioSources = GetComponents<AudioSource>();
        Music = AllAudioSources[0];
        Parry = AllAudioSources[1];
        DashAttack = AllAudioSources[2];
        Attack1 = AllAudioSources[3];
        Attack2 = AllAudioSources[4];
        Attack3 = AllAudioSources[5];
        Swing1 = AllAudioSources[6];
        Swing2 = AllAudioSources[7];
        Ground = AllAudioSources[8];
        JumpDamage = AllAudioSources[9];
        FireHit = AllAudioSources[10];
        Eat = AllAudioSources[11];
        DamageUI = Resources.Load<GameObject>("Prefabs/DamageUI");
        Canvas = GameObject.Find("Canvas");
    }

    // Update is called once per frame
    void Update()
    {

    }
    void FixedUpdate() {
        PauseTimer++;
        if (PauseTimer >= Pause) {
            for (int n = 0; n < Playing.Count; n++) {
                Playing[n].Play();
                PlayAmount[n] += 1;
                if (PlayAmount[n] >= 2) {
                    Playing.Remove(Playing[n]);
                    PlayAmount.Remove(PlayAmount[n]);
                }
            }
            PauseTimer = 0;
        }
        foreach (GameObject UI in DamageUIs) {
            UI.transform.position = new Vector2(UI.transform.position.x, UI.transform.position.y + 0.5f);
            Color TextColor = UI.GetComponent<Text>().color;
            UI.GetComponent<Text>().color = new Color(TextColor.r, TextColor.g, TextColor.b, TextColor.a - 0.025f);
            if (TextColor.a <= 0) {
                GameObject ToBeDestroyed = UI;
                DamageUIs.Remove(UI);
                Destroy(ToBeDestroyed);
                break;
            }
        }
    }
    public static void Play2Times(AudioSource Play) {
        Playing.Add(Play);
        PlayAmount.Add(Playing.Count - 1);
    }
    public static void ShowDamage(Vector2 Pos, int num) {
        ShowDamage(Pos, num, false);
    }
    public static void ShowDamage(Vector2 Pos, int num, bool Reverse) {
        GameObject Number = Instantiate(DamageUI) as GameObject;
        if (Reverse == true) {
            Number.GetComponent<Text>().color = new Color(0, 255, 0, 1);
        }
        RectTransform RectTrans = Number.GetComponent<RectTransform>();
        Number.transform.SetParent(Canvas.transform);
        RectTrans.rect.Set(0, 0, 0, 0);
        RectTrans.anchoredPosition = Vector2.zero;
        RectTrans.anchorMin = Camera.main.WorldToViewportPoint(Pos);
        RectTrans.anchorMax = Camera.main.WorldToViewportPoint(Pos);
        Number.GetComponent<Text>().text = "" + num;
        Number.GetComponent<Text>().fontSize = 20 + num;
        DamageUIs.Add(Number);
    }
}

