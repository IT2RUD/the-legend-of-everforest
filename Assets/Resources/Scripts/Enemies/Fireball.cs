﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour {
    Rigidbody2D FireBody;
    GameObject Manager;
    // Use this for initialization
    void LateUpdate() {
        ParticleSystem.Particle[] p = new ParticleSystem.Particle[GetComponent<ParticleSystem>().particleCount + 1];
        int l = GetComponent<ParticleSystem>().GetParticles(p);
        int i = 0;
        while (i < l) {
            p[i].velocity = -(FireBody.velocity.normalized) * 7;
            i++;
        }
        GetComponent<ParticleSystem>().SetParticles(p, l);
        Manager = GameObject.FindGameObjectWithTag("Manager");
    }
    void OnTriggerEnter2D(Collider2D other) {
        FireBody = GetComponent<Rigidbody2D>();
        if (other.tag == "PlayerShield") {
            GameManager.Parry.Play();
            Destroy(gameObject);
            return;
        }
        if (other.tag == "PlayerHitBox") {
            GameObject PlayerHit = other.transform.parent.gameObject;
            Movement Movement = PlayerHit.GetComponent<Movement>();
            if (Movement.CurrentAnimation == Movement.MyAnimations.BLOCK) {
                GameManager.Parry.Play();
                Destroy(gameObject);
                return;
            }
            if (Movement.Disabled == false) {
                if (Movement.TurnedLeft == true && Mathf.Sign(FireBody.velocity.x) == -1) {
                    Movement.MoveRight();
                }
                if (Movement.TurnedRight == true && Mathf.Sign(FireBody.velocity.x) == 1) {
                    Movement.MoveLeft();
                }
                Movement.DisabledAmount = 40;
                Movement.Disabled = true;
                if (PlayerHit.GetComponent<Health>().PlayerHealth > 0) {
                    PlayerHit.GetComponent<Health>().PlayerHealth -= 15;
                }
                PlayerHit.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                PlayerHit.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Sign(FireBody.velocity.x), 0.8f) * 230f);
                if (Mathf.Sign(FireBody.velocity.x) == -1)
                    GameManager.ShowDamage(new Vector2(PlayerHit.transform.position.x - 0.15f, PlayerHit.transform.position.y + 0.2f), 15);
                if (Mathf.Sign(FireBody.velocity.x) == 1)
                    GameManager.ShowDamage(new Vector2(PlayerHit.transform.position.x + 0.15f, PlayerHit.transform.position.y + 0.2f), 15);
                GameManager.FireHit.Play();
            }
            Destroy(gameObject);
        }
        if (other.tag == "Floor") {
            Destroy(gameObject);
            return;
        }
    }
}
