﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class EnemyMovement : MonoBehaviour {
    public GameObject Player;
    public GameObject Fireball;
    public GameObject Blood;
    public CircleCollider2D Friction;
    public CircleCollider2D NoFriction;

    public float Speed;
    public float Cooldown;

    public bool IAmGomba;
    public bool IAmRabbit;

    bool DoAttack;
    bool AttackDone;

    int AttackTimer;

    SpriteRenderer EnemySprite;

    Sprite[] EnemyMoving;
    Sprite[] EnemyDamaged;
    Sprite[] EnemyAttack;
    Sprite[] EnemyJumpUp;
    Sprite[] EnemyJumpDown;
    Sprite[] EnemyJumpStart;
    Sprite[] EnemyIdle;
    Sprite[] EnemyStart;

    Rigidbody2D EnemyBody;

    float FireBallRange = 6;
    float AnimPause;
    float PauseTimer;
    float CurrentCooldown;
    float MoveCheck;
    float MyPosX;
    float DisabledAmount;

    public GameObject[] Players;

    float[] Length;

    int AnimTimer;

    bool Disabled;

    [HideInInspector]
    public bool FloorAir;
    [HideInInspector]
    public bool FloorGround;
    bool OnAir;
    bool OnGround;
    bool Jump;
    bool DoJump;
    int JumpTimer;
    bool JumpDone;

    float ComboCounter;

    public float FullHealth;
    public float CurrentHealth;

    string Enemy;

    bool Moving;
    bool Idle;

    bool StartDone;
    bool DoStart;

    // Use this for initialization
    void Start() {
        if (IAmGomba == false && IAmRabbit == false) {
            Debug.Log("You forgot to add type to monster");
        }
        if (IAmGomba == true) {
            FullHealth = 45;
            Enemy = "Gomba";
            EnemyAttack = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Attack");
            StartDone = true;
            Moving = true;
        }
        if (IAmRabbit == true) {
            FullHealth = 30;
            Enemy = "Rabbit";
            EnemyIdle = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Idle");
            EnemyStart = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Start");
        }
        CurrentHealth = FullHealth;
        EnemySprite = GetComponent<SpriteRenderer>();
        EnemyMoving = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Moving");
        EnemyDamaged = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Damaged");
        EnemyJumpUp = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Jump/JumpUp");
        EnemyJumpDown = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Jump/JumpDown");
        EnemyJumpStart = Resources.LoadAll<Sprite>("Sprites/Enemies/" + Enemy + "/Jump/JumpStart");

        EnemyBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if (EnemyBody.velocity.x < 0.5f && DoAttack == false && Disabled == false && StartDone == true) {
            Friction.enabled = false;
            NoFriction.enabled = true;
        } else {
            Friction.enabled = true;
            NoFriction.enabled = false;
        }
        if (Player.transform.position.x < transform.position.x) {
            EnemyBody.transform.localEulerAngles = new Vector3(0, 180, 0);
        }
        if (Player.transform.position.x > transform.position.x) {
            EnemyBody.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }
    void FixedUpdate() {
        if (CurrentHealth <= 0) {
            EnemySprite.sprite = EnemyDamaged[0];
            EnemySprite.color = new Color(EnemySprite.color.r, EnemySprite.color.g, EnemySprite.color.b, EnemySprite.color.a - 0.05f);
            if (EnemySprite.color.a <= 0) {
                Destroy(gameObject);
            }
        }
        CheckGroundAir();
        Cooldowns();
        DoAnimation();
        if (DoStart == true)
            return;
        if (DoAttack == true) {
            ReCalculate();
        }
        if (Disabled == false) {
            if (StartDone == true) {
                MaybeJump();
            }
            if ((Player.transform.position.x < transform.position.x + FireBallRange && Player.transform.position.x > transform.position.x - FireBallRange && CurrentCooldown == 0) || DoAttack == true) {
                if (Player.transform.position.y < transform.position.y + 2f && Player.transform.position.y > transform.position.y - 2f) {
                    if (IAmGomba == true) {
                        GombaAttack();
                        return;
                    }
                } else {
                    AttackDone = false;
                    DoAttack = false;
                }
            }

            if (IAmRabbit) {
                if ((Player.transform.position.x < transform.position.x + FireBallRange && Player.transform.position.x > transform.position.x - FireBallRange)) {
                    if (StartDone == false) {
                        DoStart = true;
                        return;
                    }
                    if (OnGround == true && DoJump == false) {
                        Move();
                    }
                    Idle = false;
                    Moving = true;
                } else {
                    if (StartDone == true) {
                        EnemyBody.velocity = new Vector2(0, EnemyBody.velocity.y);
                        Idle = true;
                        Moving = false;
                    }
                }
            }
            if (IAmGomba) {
                if ((Player.transform.position.x < transform.position.x + FireBallRange * 2.5f && Player.transform.position.x > transform.position.x - FireBallRange * 2.5f)) {
                    Move();
                }
            }
        }
    }
    void MaybeJump() {
        if (IAmGomba == true) {
            if ((Player.transform.position.y > transform.position.y + 2f && Jump == false && CurrentHealth == FullHealth) || DoJump == true) {
                if (DoJump == false) {
                    DoJump = true;
                    JumpTimer = 0;
                }
                if (JumpDone == true) {
                    Jump = true;
                    EnemyBody.velocity = new Vector2(EnemyBody.velocity.x, 0f);
                    EnemyBody.AddForce(Vector2.up * 300f); //350f
                    JumpDone = false;
                    DoJump = false;
                }
            }
        }
        if (IAmRabbit == true) {
            if ((Jump == false && Moving == true && Player.transform.position.x < transform.position.x + FireBallRange * 0.7f && Player.transform.position.x > transform.position.x - FireBallRange * 0.7f) || DoJump == true) {
                if (DoJump == false) {
                    DoJump = true;
                    JumpTimer = 0;
                    EnemyBody.velocity = new Vector2(0, EnemyBody.velocity.y);
                }
                if (JumpDone == true) {
                    Jump = true;
                    EnemyBody.velocity = new Vector2(EnemyBody.velocity.x, 0f);
                    EnemyBody.AddForce(new Vector2 (Mathf.Sign(Player.transform.position.x - MyPosX) * 0.5f, 1) * 300f); //350f
                    JumpDone = false;
                    DoJump = false;
                }
            }
        }

    }
    void Cooldowns() {
        if (CurrentCooldown > 0)
            CurrentCooldown--;
        if (DisabledAmount > 0) {
            DisabledAmount--;
        } else {
            Disabled = false;
        }
        if (Disabled == false) {
            ComboCounter = 0;
            if (MoveCheck > 0) {
                MoveCheck--;
            } else {
                ReCalculate();
                MoveCheck = 10;
            }
        } else {
            DoJump = false;
            DoAttack = false;
        }
    }
    void CheckGroundAir() {
        if (FloorAir == true) {
            FloorAir = false;
            OnAir = false;
            OnGround = true;
            Jump = false;
        }
        if (FloorGround == true) {
            FloorGround = false;
            OnAir = true;
            OnGround = false;
        }
    }
    void DoAnimation() {
        PauseTimer++;
        if (Disabled == true) {
            if (PauseTimer >= AnimPause) {
                AnimTimer = AnimTimer > EnemyDamaged.Length - 2 ? 0 : AnimTimer + 1;
                PauseTimer = 0;
                EnemySprite.sprite = EnemyDamaged[AnimTimer];
            }
        }
        if (Disabled == false) {
            if (DoStart == true) {
                AnimPause = 3f;
                if (AttackTimer < EnemyStart.Length) {
                    if (PauseTimer >= AnimPause) {
                        EnemySprite.sprite = EnemyStart[AttackTimer];
                        AttackTimer++;
                        PauseTimer = 0;
                    }
                } else {
                    DoStart = false;
                    StartDone = true;
                }
            } else if (DoAttack == true) {
                AnimPause = 3f;
                DoJump = false;
                if (AttackTimer < EnemyAttack.Length) {
                    if (PauseTimer >= AnimPause) {
                        EnemySprite.sprite = EnemyAttack[AttackTimer];
                        AttackTimer++;
                        PauseTimer = 0;
                    }
                } else {
                    AttackDone = true;
                }
            } else if (DoJump == true) {
                AnimPause = 3f;
                if (JumpTimer < EnemyJumpStart.Length) {
                    if (PauseTimer >= AnimPause) {
                        EnemySprite.sprite = EnemyJumpStart[JumpTimer];
                        JumpTimer++;
                        PauseTimer = 0;
                    }
                } else {
                    JumpDone = true;
                }
            } else if (OnGround == true) {
                if (Idle == true) {
                    AnimPause = 10f;
                    if (PauseTimer >= AnimPause) {
                        AnimTimer = AnimTimer > EnemyIdle.Length - 2 ? 0 : AnimTimer + 1;
                        PauseTimer = 0;
                        EnemySprite.sprite = EnemyIdle[AnimTimer];
                    }
                }
                if (Moving == true) {
                    AnimPause = 3f;
                    if (PauseTimer >= AnimPause) {
                        AnimTimer = AnimTimer > EnemyMoving.Length - 2 ? 0 : AnimTimer + 1;
                        PauseTimer = 0;
                        EnemySprite.sprite = EnemyMoving[AnimTimer];
                    }
                }
            } else if (OnAir == true && StartDone == true) {
                AnimPause = 1f;
                if (PauseTimer >= AnimPause) {
                    PauseTimer = 0;
                    if (EnemyBody.velocity.y > 0) {
                        AnimTimer = AnimTimer > EnemyJumpUp.Length - 2 ? 0 : AnimTimer + 1;
                        EnemySprite.sprite = EnemyJumpUp[AnimTimer];
                    }
                    if (EnemyBody.velocity.y < 0) {
                        AnimTimer = AnimTimer > EnemyJumpDown.Length - 2 ? 0 : AnimTimer + 1;
                        EnemySprite.sprite = EnemyJumpDown[AnimTimer];
                    }
                }
            }
        }
    }
    void GombaAttack() {
        if (DoAttack == false) {
            DoAttack = true;
            AttackTimer = 0;
        }
        if (AttackDone == true) {
            if (IAmGomba == true) {
                GameObject Fire = Instantiate(Fireball, transform.position, Quaternion.identity) as GameObject;
                Fire.GetComponent<Rigidbody2D>().AddForce(new Vector2(Player.transform.position.x - transform.position.x, 0).normalized * 30f);
                Destroy(Fire, 3f);
                CurrentCooldown = Cooldown;
            }
            AttackDone = false;
            DoAttack = false;
        }

    }
    void Move() {
        EnemyBody.velocity = new Vector2(Mathf.Sign(Player.transform.position.x - MyPosX) * Speed, EnemyBody.velocity.y);
    }
    void ReCalculate() {
        //Find nearest Player and gets X position
        Players = GameObject.FindGameObjectsWithTag("Player");
        Length = new float[Players.Length];
        for (int n = 0; n < Players.Length; n++)
            Length[n] = (Players[n].transform.position - transform.position).magnitude;
        Player = Players[Length.ToList().IndexOf(Length.Min())];
        MyPosX = transform.position.x;

    }
    void OnTriggerEnter2D(Collider2D other) {
        if (CurrentHealth > 0) {
            if (other.tag == "PlayerHitBox") {
                GameObject PlayerHit = other.transform.parent.gameObject;
                Movement Movement = PlayerHit.GetComponent<Movement>();
                if (Movement.Disabled == false && Disabled == false) {
                    if (PlayerHit.transform.position.y - 1f > transform.position.y) {
                        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                        Disabled = true;
                        DisabledAmount = 20;
                        GameManager.ShowDamage(transform.position, 15);
                        CurrentHealth -= 15;
                        EnemyBody.velocity = new Vector2(0, 0);
                        PlayerHit.GetComponent<Rigidbody2D>().velocity = new Vector2(PlayerHit.GetComponent<Rigidbody2D>().velocity.x, 0f);
                        PlayerHit.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 200f); //350f
                        GameManager.JumpDamage.Play();
                    }
                    return;
                }
            }
            if (other.tag == "PlayerSword") {
                Movement Movement = other.transform.parent.GetComponent<Movement>();
                if (!(Movement.Damaged.Contains(gameObject))) {
                    if (Movement.CurrentAnimation == Movement.MyAnimations.DASHATTACK) {
                        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                        float Direction = 0;
                        if (Movement.TurnedLeft == true) {
                            EnemyBody.transform.localEulerAngles = new Vector3(0, 0, 0);
                            Direction = -1;
                        }
                        if (Movement.TurnedRight == true) {
                            EnemyBody.transform.localEulerAngles = new Vector3(0, 180, 0);
                            Direction = 1;
                        }
                        GetComponent<Rigidbody2D>().AddForce(new Vector2(1.2f * Direction, 1) * 200f);
                        Disabled = true;
                        DisabledAmount = 60;
                        Movement.Damaged.Add(gameObject);
                        GameManager.Play2Times(GameManager.DashAttack);
                        GameManager.ShowDamage(transform.position, 7);
                        CurrentHealth -= 7;
                        Destroy(Instantiate(Blood, new Vector2(transform.position.x + 0.5f * Direction, transform.position.y), Quaternion.identity), 0.20f);
                    }
                    if (Movement.CurrentAnimation == Movement.MyAnimations.ATTACK) {
                        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                        float Direction = 0;
                        if (Movement.TurnedLeft == true) {
                            EnemyBody.transform.localEulerAngles = new Vector3(0, 0, 0);
                            Direction = -1;
                        }
                        if (Movement.TurnedRight == true) {
                            EnemyBody.transform.localEulerAngles = new Vector3(0, 180, 0);
                            Direction = 1;
                        }

                        if (Movement.AnimFrame > 6 && Movement.AnimFrame < 19) {
                            Disabled = true;
                            if (ComboCounter == 0) {
                                ComboCounter = 1;
                                DisabledAmount = 30;
                            } else {
                                DisabledAmount = 5;
                            }
                            GetComponent<Rigidbody2D>().AddForce(new Vector2(1.2f * Direction, 1) * 70f);
                            GameManager.Attack1.Play();
                            GameManager.ShowDamage(transform.position, 5);
                            CurrentHealth -= 5;
                            Destroy(Instantiate(Blood, transform.position, Quaternion.identity), 0.25f);
                        } else if (Movement.AnimFrame > 26 && Movement.AnimFrame < 44) {
                            Disabled = true;
                            if (ComboCounter == 1) {
                                ComboCounter = 2;
                                DisabledAmount = 40;
                            } else {
                                DisabledAmount = 5;
                            }
                            GetComponent<Rigidbody2D>().AddForce(new Vector2(1.2f * Direction, 1) * 65f);
                            GameManager.Attack2.Play();
                            GameManager.ShowDamage(transform.position, 6);
                            CurrentHealth -= 6;
                            Destroy(Instantiate(Blood, transform.position, Quaternion.identity), 0.25f);
                        } else if (Movement.AnimFrame > 50 && Movement.AnimFrame < 60) {
                            Disabled = true;
                            DisabledAmount = 45;
                            ComboCounter = 0;
                            GetComponent<Rigidbody2D>().AddForce(new Vector2(1.2f * Direction, 1) * 135f);
                            GameManager.Attack3.Play();
                            GameManager.ShowDamage(transform.position, 12);
                            CurrentHealth -= 12;
                            Destroy(Instantiate(Blood, new Vector2(transform.position.x + 0.3f * Direction, transform.position.y), Quaternion.identity), 0.25f);
                        }
                        Movement.Damaged.Add(gameObject);
                    }
                }
            }
        }
    }
    void OnTriggerStay2D(Collider2D other) {
        if (CurrentHealth > 0) {
            if (other.tag == "PlayerHitBox") {
                GameObject PlayerHit = other.transform.parent.gameObject;
                Movement Movement = PlayerHit.GetComponent<Movement>();
                if (Movement.Disabled == false && Movement.Invulnerable == 0 && Disabled == false) {
                    if (Movement.TurnedLeft == true && Mathf.Sign(EnemyBody.velocity.x) == -1) {
                        Movement.MoveRight();
                    }
                    if (Movement.TurnedRight == true && Mathf.Sign(EnemyBody.velocity.x) == 1) {
                        Movement.MoveLeft();
                    }
                    if (IAmRabbit == true)
                        Movement.DisabledAmount = 30;
                    if (IAmGomba == true)
                         Movement.DisabledAmount = 40;
                    Movement.Disabled = true;
                    if (PlayerHit.GetComponent<Health>().PlayerHealth > 0) {
                        PlayerHit.GetComponent<Health>().PlayerHealth -= 10;
                    }
                    PlayerHit.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    if (IAmRabbit == true)
                        PlayerHit.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Sign(EnemyBody.velocity.x), 0.8f) * 100f);
                    if (IAmGomba == true)
                        PlayerHit.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Sign(EnemyBody.velocity.x), 0.8f) * 230f);

                    if (Mathf.Sign(EnemyBody.velocity.x) == -1)
                        GameManager.ShowDamage(new Vector2(PlayerHit.transform.position.x - 0.15f, PlayerHit.transform.position.y + 0.2f), 10);
                    if (Mathf.Sign(EnemyBody.velocity.x) == 1)
                        GameManager.ShowDamage(new Vector2(PlayerHit.transform.position.x + 0.15f, PlayerHit.transform.position.y + 0.2f), 10);
                    if (IAmRabbit == true)
                        Movement.Invulnerable = 70;
                    if (IAmGomba == true)
                        Movement.Invulnerable = 75;

                }
            }
        }
    }
}
