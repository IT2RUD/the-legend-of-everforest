﻿using UnityEngine;
using System.Collections;

public class EnemyFloorDetector : MonoBehaviour {
    GameObject Enemy;
    bool Done;
    void Start() {
        if (Done == false) {
            Enemy = transform.parent.gameObject;
            Done = true;
            Enemy.GetComponent<EnemyMovement>().FloorGround = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (Done == false) {
            Enemy = transform.parent.gameObject;
            Done = true;
        }
        if (other.tag == "Floor") {
            Enemy.GetComponent<EnemyMovement>().FloorAir = true;
        }
    }
    void OnTriggerExit2D(Collider2D other) {
        if (Done == false) {
            Enemy = transform.parent.gameObject;
            Done = true;
        }
        if (other.tag == "Floor") {
            Enemy.GetComponent<EnemyMovement>().FloorGround = true;
        }
    }
}
