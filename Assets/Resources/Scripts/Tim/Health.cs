﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {
    public GameObject HealthContainer;
    public GameObject HealthBar;
    public float PlayerMaxHealth = 100;
    public float PlayerHealth;
	// Use this for initialization
	void Start () {
        PlayerHealth = PlayerMaxHealth;
	}
	
	// Update is called once per frame
	void Update () {
        HealthContainer.transform.position = new Vector3(transform.position.x, transform.position.y + 0.9f, transform.position.z);
        HealthBar.transform.localPosition = new Vector3((PlayerHealth - PlayerMaxHealth)/(PlayerMaxHealth * 2f), 0, 0);
        HealthBar.transform.localScale = new Vector3(PlayerHealth/ PlayerMaxHealth, 1, 1);
    }
}
