﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Movement : MonoBehaviour {
    #region Variables

    public GameObject Sword;
    public GameObject SwordHand;
    public GameObject JumpCircle;
    public GameObject MainCamera;
    public GameObject AttackPart;
    public GameObject Smoke;
    public GameObject Shield;
    public GameObject AttackHitBox;

    public CircleCollider2D Friction;
    public CircleCollider2D NoFriction;

    [HideInInspector]
    public List<GameObject> Damaged = new List<GameObject>();

    [HideInInspector]
    public enum MyAnimations {
        NOTHING, IDLE, MOVEMENT, JUMPUP, JUMPDOWN, PREDASH, DASHATTACK, ATTACK, BLOCK, PREDAMAGED, DAMAGED, CLIMB
    }
    [HideInInspector]
    public MyAnimations CurrentAnimation;
    [HideInInspector]
    public bool TurnedLeft;
    [HideInInspector]
    public bool TurnedRight = true;

    [HideInInspector]
    public bool FloorAir;
    [HideInInspector]
    public bool FloorGround;
    [HideInInspector]
    public bool Disabled;
    [HideInInspector]
    public float DisabledAmount;
    [HideInInspector]
    public float DisabledTimer;
    [HideInInspector]
    public float Invulnerable;


    GameObject CurrentJumpCircle;
    GameObject CurrentShield;
    GameObject CurrentAttack;

    float Move;                     //The current amount the player moves.
    float SpeedMultiplier = 5f;     //To adjust the speed.
    float AccelSpeed = 2f;          //The speed of which the StartSpeed reaches EndSpeed
    float StartSpeed = 0.5f;        //The starting speed before the speed accelerates. Either 0f or 0.5f depending, IMO.
    float EndSpeed = 1f;            //Max speed the player can accelerate to.
    float AnimPause = 5f;
    float GrowthSizeCircle = 0.1f;
    float MaxSizeCircle = 0.5f;
    float Timer;
    float PauseTimer;
    float AnimDuration;
    float JumpDashDuration;
    float PreDashDuration;
    float JumpDashMax = 5;
    float Turn;
    float Cooldown;
    float DashCooldown;
    public float AttackCombo;
    float GetUpTimer;
    float GetUpDuration = 20f;

    int AnimTimer;
    int SwordNum;
    public int AnimFrame;
    int BlockTimer;

    bool Idle = true;
    bool Moving;
    bool Jump;
    bool ExtraJump;
    bool OnAir;
    bool OnGround;
    bool PlayerAnim;
    bool AnimDone;
    bool FallAnimation = true;
    bool EndingAnimation;
    bool StartDone;
    bool AnimCountdown;
    bool PreDamagedDone;
    bool DisabledStart;
    bool StopMove;

    Vector3 SwordStartingPosition;
    Vector3 SwordStartingRotation;
    Vector3 Position;

    SpriteRenderer TimSprite;
    SpriteRenderer SwordSprite;
    SpriteRenderer HandSprite;
    SpriteRenderer AttackSprite;

    Sprite[] TimIdle;
    Sprite[] TimMovement;
    Sprite[] TimJumpUp;
    Sprite[] TimJumpDown;
    Sprite[] TimPreDash;
    Sprite[] TimDashAttack;
    Sprite[] TimAttack;
    Sprite[] TimBlock;
    Sprite[] TimPreDamaged;
    Sprite[] TimDamaged;
    Sprite[] TimClimb;
    Sprite[] Swords;

    Rigidbody2D PlayerBody;

    KeyCode KeyLeft;
    KeyCode KeyRight;
    KeyCode KeyJump;
    KeyCode KeyJump2;
    KeyCode KeyAttack;
    KeyCode KeyDown;
    KeyCode KeyBlock;

    #endregion
    #region TestVariables
    public bool CreateTestObject;
    public bool TestObject;

    int TestValue = -1;

    #endregion
    // Use this for initialization
    void Start() {
        SwordStartingPosition = Sword.transform.localPosition;
        SwordStartingRotation = Sword.transform.localEulerAngles;

        PlayerBody = GetComponent<Rigidbody2D>();

        TimSprite = GetComponent<SpriteRenderer>();
        SwordSprite = Sword.GetComponent<SpriteRenderer>();
        HandSprite = SwordHand.GetComponent<SpriteRenderer>();
        AttackSprite = AttackPart.GetComponent<SpriteRenderer>();

        TimIdle = Resources.LoadAll<Sprite>("Sprites/Tim/TimIdle");
        TimMovement = Resources.LoadAll<Sprite>("Sprites/Tim/TimMovement/");
        TimPreDash = Resources.LoadAll<Sprite>("Sprites/Tim/TimAttacks/TimDashAttack/PreDash");
        TimJumpUp = Resources.LoadAll<Sprite>("Sprites/Tim/TimJump/TimJumpUp");
        TimJumpDown = Resources.LoadAll<Sprite>("Sprites/Tim/TimJump/TimJumpDown");
        TimDashAttack = Resources.LoadAll<Sprite>("Sprites/Tim/TimAttacks/TimDashAttack/Dash");
        TimAttack = Resources.LoadAll<Sprite>("Sprites/Tim/TimAttacks/TimAttack/");
        TimBlock = Resources.LoadAll<Sprite>("Sprites/Tim/TimAttacks/TimBlock/");
        TimPreDamaged = Resources.LoadAll<Sprite>("Sprites/Tim/TimDamaged/PreDamaged");
        TimDamaged = Resources.LoadAll<Sprite>("Sprites/Tim/TimDamaged/Damaged");
        TimClimb = Resources.LoadAll<Sprite>("Sprites/Tim/TimClimb");
        Swords = Resources.LoadAll<Sprite>("Sprites/Swords");

        KeyLeft = KeyCode.LeftArrow;
        KeyRight = KeyCode.RightArrow;
        KeyJump = KeyCode.Space;
        KeyJump2 = KeyCode.UpArrow;
        KeyAttack = KeyCode.A;
        KeyDown = KeyCode.DownArrow;
        KeyBlock = KeyCode.S;

        if (TestObject == true) {
            KeyLeft = KeyCode.J;
            KeyRight = KeyCode.L;
            KeyJump = KeyCode.I;
            KeyJump2 = KeyCode.ScrollLock;
            KeyDown = KeyCode.K;
        }
        Reset();
    }
    void FixedUpdate() {
        #region TestButtons
        if (Input.GetKey(KeyCode.U)) {
            Sprite[] TestingSprite = TimBlock;
            MyAnimations AnimationName = MyAnimations.BLOCK;
            TestValue = TestValue > TestingSprite.Length - 2 ? 0 : TestValue + 1;
            Debug.Log(TestValue);
            PlayAnimation(TestingSprite[TestValue], AnimationName, TestValue);
        }
        #endregion
        #region TestFunctions
        if (TestValue != -1)
            return;
        #endregion
        #region Timers
        Timer += Time.deltaTime;
        if (Disabled == true) {
            DisabledTimer++;
            if (DisabledTimer > 10) {
                if (OnGround == true) {
                    GetUpTimer++;
                    if (GetUpTimer > GetUpDuration) {
                        PreDamagedDone = false;
                        Disabled = false;
                        DisabledStart = false;
                        GetUpTimer = 0;
                        DisabledTimer = 0;
                        Reset();
                    }
                }
            }
            if (DisabledTimer > DisabledAmount && OnGround == false) {
               PreDamagedDone = false;
               Disabled = false;
               DisabledStart = false;
               GetUpTimer = 0;
               DisabledTimer = 0;
               Reset();
            }
        }
        if (Cooldown > 0)
            Cooldown--;
        if (DashCooldown > 0)
            DashCooldown--;
        if (Invulnerable> 0)
            Invulnerable--;
        #endregion
        #region Movement
        if (PlayerAnim == false && Disabled == false) {
            if (OnGround == true) {
                if (Moving == true && Mathf.Abs(PlayerBody.velocity.x) < 0.5f) {
                    NoFriction.enabled = true;
                    Friction.enabled = false;
                } else {
                    Friction.enabled = true;
                    NoFriction.enabled = false;
                }
                PlayerBody.velocity = new Vector2(Move * SpeedMultiplier, PlayerBody.velocity.y);
            }
            if (OnAir == true) {
                NoFriction.enabled = true;
                Friction.enabled = false;
                PlayerBody.velocity = new Vector2(Move * SpeedMultiplier/1.5f, PlayerBody.velocity.y);
            }
        }
        #endregion
        if (CurrentJumpCircle != null) {
            CurrentJumpCircle.transform.localScale = new Vector3(CurrentJumpCircle.transform.localScale.x + GrowthSizeCircle, CurrentJumpCircle.transform.localScale.y + GrowthSizeCircle, 1);
            if (CurrentJumpCircle.transform.localScale.x > MaxSizeCircle)
                Destroy(CurrentJumpCircle);
        }
        DoAnimation();
    } 
    void Update() {
        MainCamera.transform.position = new Vector3(transform.position.x, MainCamera.transform.position.y, -10);
        #region TestFunctions
        if (TestObject == false && CreateTestObject == true) {
            GameObject TestPlayer = Instantiate(gameObject, transform.position, transform.rotation) as GameObject;
            TestPlayer.GetComponent<Movement>().TestObject = true;
            CreateTestObject = false;
        }
        #endregion
        #region TestButtons
        if (TestObject == false) {
            if (Input.GetKeyDown(KeyCode.E)) {
                Sprite[] TestingSprite = TimClimb;
                MyAnimations AnimationName = MyAnimations.CLIMB;
                TestValue = TestValue > TestingSprite.Length - 2 ? 0 : TestValue + 1;
                Debug.Log(TestValue);
                PlayAnimation(TestingSprite[TestValue], AnimationName, TestValue);
            }
            if (Input.GetKeyDown(KeyCode.R)) {
                SwordNum = SwordNum > Swords.Length - 2 ? 0 : SwordNum + 1;
                Sword.GetComponent<SpriteRenderer>().sprite = Swords[SwordNum];
            }
            if (Input.GetKeyDown(KeyCode.T)) {
                if (Time.timeScale == 1f) {
                    Time.timeScale = 0.25f;
                    return;
                }
                if (Time.timeScale == 0.25f) {
                    Time.timeScale = 0.1f;
                    return;
                }
                if (Time.timeScale == 0.1f) {
                    Time.timeScale = 1f;
                    return;
                }
            }
            if (Input.GetKeyDown(KeyCode.Y)) {
                Time.timeScale = Time.timeScale != 0 ? 0 : 1f;
            }
            if (Input.GetKey(KeyCode.I)) {
                Destroy(GameObject.Find("Enemies"));
            }
            if (TestValue != -1 || Time.timeScale == 0)
                return;
        }
        #endregion
        #region FloorDetector
        //Checks if Player is on the ground or in the air.
        if (FloorAir == true) {
            FloorAir = false;
            Jump = false;
            ExtraJump = false;
            OnAir = false;
            OnGround = true;
            if (PlayerAnim == false && Disabled == false)
                Reset();
        }
        if (FloorGround == true) {
            FloorGround = false;
            OnAir = true;
            OnGround = false;
        }
        #endregion
        #region Buttons
        if (PlayerAnim == false && Disabled == false) {
            if (Input.GetKey(KeyDown)) {
                PlayerBody.gravityScale = 2;
            } else {
                PlayerBody.gravityScale = 1;
            }
            if (Input.GetKeyDown(KeyJump) || Input.GetKeyDown(KeyJump2)) {
                DoJump();
            }
            if (Input.GetKey(KeyLeft) && !Input.GetKey(KeyRight)) {
                MoveLeft();
            }
            if (Input.GetKey(KeyRight) && !Input.GetKey(KeyLeft)) {
                MoveRight();
            }
            if ((Input.GetKey(KeyLeft) && Input.GetKey(KeyRight)) || (!Input.GetKey(KeyLeft) && !Input.GetKey(KeyRight))) {
                if (Move != 0 || Moving == true) {
                    Idle = true;
                    Moving = false;
                    AnimPause = 5;
                    Reset();
                }
            }
            if (Input.GetKeyDown(KeyAttack) && Cooldown == 0) {
                if (Mathf.Abs(Move) > 0.9f && Mathf.Abs(PlayerBody.velocity.x) > 0.9f && OnGround == true && DashCooldown == 0) {
                    if (Input.GetKey(KeyLeft) || Input.GetKey(KeyRight)) {
                        FrameAnimation(MyAnimations.DASHATTACK);
                    }
                } else {
                    FrameAnimation(MyAnimations.ATTACK);
                }
            }
            if (Input.GetKeyDown(KeyBlock) && Cooldown == 0) {
                //FrameAnimation(MyAnimations.BLOCK);
            }
        }
        if (CurrentAnimation == MyAnimations.ATTACK) {
            if (Input.GetKeyDown(KeyAttack)) {
                if (AnimFrame >= 1 && AnimFrame <= 22)
                    AttackCombo = 1;
                if (AnimFrame >= 23 && AnimFrame <= 43)
                    AttackCombo = 2;
            }
        }
        #endregion
    }
    public void MoveLeft() {
        if (Idle == true || TurnedRight == true) {
            Timer = 0;
            AnimPause = 1;
            Reset();
            Idle = false;
            Moving = true;
        }
        if (TurnedRight == true) {
            TurnedRight = false;
            TurnedLeft = true;
            transform.Rotate(0, 180, 0);
        }
        Move = -StartSpeed - Timer * AccelSpeed;
        Move = Mathf.Max(-EndSpeed, Move);
    }
    public void MoveRight() {
        if (Idle == true || TurnedLeft == true) {
            Timer = 0;
            AnimPause = 1;
            Reset();
            Idle = false;
            Moving = true;
        }
        if (TurnedLeft == true) {
            TurnedLeft = false;
            TurnedRight = true;
            transform.Rotate(0, 180, 0);
        }
        Move = StartSpeed + Timer * AccelSpeed;
        Move = Mathf.Min(EndSpeed, Move);
    }
   void DoJump() {
        if (Jump == false || ExtraJump == false) {
            if (OnAir == true) {
                ExtraJump = true;
                if (CurrentJumpCircle != null)
                    Destroy(CurrentJumpCircle);
                CurrentJumpCircle = Instantiate(JumpCircle, new Vector2(transform.position.x, transform.position.y - 1.37f / 2f), Quaternion.Euler(80, 0, 0)) as GameObject;
            }
            Jump = true;
            PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, 0f);
            PlayerBody.AddForce(Vector2.up * 300f); //350f
        }
    }
    void FrameAnimation(MyAnimations PlayAnimation) {
        if (PlayAnimation == MyAnimations.DASHATTACK) {
            AnimCountdown = false;
            Sword.GetComponent<BoxCollider2D>().enabled = true;
        }
        else if (PlayAnimation == MyAnimations.ATTACK) {
            AnimCountdown = true;
            AnimDuration = 64;
            PlayerBody.velocity = new Vector2(0, PlayerBody.velocity.y);
            AttackCombo = 0;
        } else if (PlayAnimation == MyAnimations.CLIMB) {
            AnimCountdown = false;
        } else if (PlayAnimation == MyAnimations.BLOCK) {
            AnimCountdown = true;
            AnimDuration = 2;
            PlayerBody.velocity = new Vector2(0, 5 - 5);
            CurrentShield = Instantiate(Shield, transform.position, Quaternion.identity) as GameObject;
        }
        PlayerAnim = true;
        CurrentAnimation = PlayAnimation;
    }
    void DoAnimation() {
        PauseTimer++;
        if (Disabled == false) {
            AnimPause = Move != 0 ? 1 / Mathf.Abs(Move) : 5;
        }
        if (PlayerAnim == true) {
            if (Disabled == false) {
                #region DashAttack
                if (CurrentAnimation == MyAnimations.DASHATTACK) {
                    if (PreDashDuration < TimPreDash.Length) {
                        PlayAnimation(TimPreDash[(int)PreDashDuration], MyAnimations.PREDASH, (int)PreDashDuration);
                        PreDashDuration++;
                    } else if (JumpDashDuration < JumpDashMax) {
                        if (StartDone == false) {
                            Sword.transform.localEulerAngles = new Vector3(0, 0, 272);
                            StartDone = true;
                            GetComponent<BoxCollider2D>().enabled = false;
                            GetComponent<CircleCollider2D>().offset = new Vector2(0, 0f);
                            transform.FindChild("Collider").GetComponent<BoxCollider2D>().offset = new Vector2(0, -0.2f);
                        }
                        PlayAnimation(TimDashAttack[0], MyAnimations.DASHATTACK, 0);
                        JumpDashDuration++;
                        PlayerBody.velocity = new Vector2(Move * SpeedMultiplier * (2.8f - 1.8f * Mathf.Sqrt(JumpDashDuration / JumpDashMax)), 1.5f);
                        transform.Rotate(0, 0, -0.6f);
                        Sword.transform.Rotate(0, 0, 0.3f);
                    } else if (FallAnimation == true) {
                        JumpDashDuration++;
                        transform.Rotate(0, 0, -0.8f);
                        Sword.transform.Rotate(0, 0, 0.3f);
                        if (Mathf.Abs(PlayerBody.velocity.x) < 1 || JumpDashDuration > JumpDashMax * 5f) {
                            if (TurnedRight == true)
                                transform.localEulerAngles = new Vector3(0, 0, 0);
                            if (TurnedLeft == true)
                                transform.localEulerAngles = new Vector3(0, 180, 0);
                            Sword.transform.localScale = new Vector3(0.8f, 0.6f, 1f);
                            PlayerBody.velocity = new Vector2(Move * SpeedMultiplier, 5 - 5);
                            FallAnimation = false;
                            if (Mathf.Abs(PlayerBody.velocity.x) < 1) {
                                AnimDone = true;
                            } else {
                                EndingAnimation = true;
                                AnimDuration = 8;
                                AnimCountdown = true;
                            }
                        }
                    }
                }
                if (EndingAnimation == true) {
                    PlayAnimation(TimDashAttack[1], MyAnimations.DASHATTACK, 1);
                    transform.Rotate(0, 0, -360 / (AnimDuration));
                    if (OnGround == true)
                        PlayerBody.velocity = new Vector2(Move * SpeedMultiplier * (1 + 1.5f / Mathf.Sqrt(AnimFrame + 1)), 5 - 5);
                }
                #endregion
                #region Attack
                if (CurrentAnimation == MyAnimations.ATTACK) {
                    if (TurnedRight == true)
                        Turn = 0;
                    if (TurnedLeft == true)
                        Turn = 180;
                    if (StopMove == false) {
                        if (Input.GetKey(KeyCode.LeftArrow)) {
                            if (TurnedRight == true) {
                                TurnedLeft = true;
                                TurnedRight = false;
                                transform.Rotate(0, 180, 0);
                            }
                        }
                        if (Input.GetKey(KeyCode.RightArrow)) {
                            if (TurnedLeft == true) {
                                TurnedRight = true;
                                TurnedLeft = false;
                                transform.Rotate(0, 180, 0);
                            }
                        }
                    }
                    /* If the player hits in the air, he should stand a bit in the air when he attacks an enemy. But for now its not needed as no enemy is any big
                    Remeber:             PlayerBody.gravityScale = 0.1f; and Playerbody.velocity.y = 0;
                    if (OnAir == true) {
                        if (PlayerBody.velocity.y < -0.1f)
                            PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, -0.1f);
                        if (PlayerBody.velocity.y > 0.1f)
                            PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, 0.1f);
                        if (PlayerBody.velocity.x < -2f)
                            PlayerBody.velocity = new Vector2(-2f, PlayerBody.velocity.y);
                        if (PlayerBody.velocity.x > 2f)
                            PlayerBody.velocity = new Vector2(2f, PlayerBody.velocity.y);
                        if (Input.GetKey(KeyCode.LeftArrow))
                            PlayerBody.AddForce(new Vector2(-10, 0));
                        if (Input.GetKey(KeyCode.RightArrow))
                            PlayerBody.AddForce(new Vector2(10, 0));
                        if ((Input.GetKey(KeyLeft) && Input.GetKey(KeyRight)) || (!Input.GetKey(KeyLeft) && !Input.GetKey(KeyRight))) {
                            if (Mathf.Abs(PlayerBody.velocity.x) < 4)
                                PlayerBody.velocity = new Vector2(0, PlayerBody.velocity.y);
                        }
                    }
                    */
                    if (AnimFrame == 1) {
                        if (OnAir == false) {
                            if (TurnedLeft)
                                PlayerBody.velocity = new Vector2(-3, PlayerBody.velocity.y);
                            if (TurnedRight)
                                PlayerBody.velocity = new Vector2(3, PlayerBody.velocity.y);
                        }
                        AttackPart.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
                        PlayAnimation(TimAttack[0], MyAnimations.ATTACK, 0);
                    } else if (AnimFrame == 4)
                        PlayAnimation(TimAttack[1], MyAnimations.ATTACK, 1);
                    else if (AnimFrame == 5)
                        PlayAnimation(TimAttack[2], MyAnimations.ATTACK, 2);
                    else if (AnimFrame == 6)
                        PlayAnimation(TimAttack[3], MyAnimations.ATTACK, 3);
                    else if (AnimFrame == 7) {
                        PlayAnimation(TimAttack[4], MyAnimations.ATTACK, 4);
                        AttackSprite.sortingOrder = 1;
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(75, Turn, 225))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(75, Turn, 270))), 0.1f);
                        AttackSprite.sortingOrder = -1;
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(75, Turn, 315))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(75, Turn, 0))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(75, Turn, 45))), 0.1f);
                        Damaged.Clear();
                        GameManager.Swing1.Play();
                        if (TurnedLeft == true)
                            CurrentAttack = Instantiate(AttackHitBox, new Vector2(transform.position.x - 0.6f, transform.position.y), Quaternion.identity) as GameObject;
                        if (TurnedRight == true)
                            CurrentAttack = Instantiate(AttackHitBox, new Vector2(transform.position.x + 0.6f, transform.position.y), Quaternion.identity) as GameObject;
                        CurrentAttack.transform.parent = transform;
                        Destroy(CurrentAttack, 0.1f);
                        if (OnAir == false)
                            PlayerBody.velocity = new Vector2(0, PlayerBody.velocity.y);
                    } else if (AnimFrame == 19) {
                        if (OnAir == false) {
                            if (TurnedLeft)
                                PlayerBody.velocity = new Vector2(-5, PlayerBody.velocity.y);
                            if (TurnedRight)
                                PlayerBody.velocity = new Vector2(5,  PlayerBody.velocity.y);
                        }
                        PlayAnimation(TimAttack[5], MyAnimations.ATTACK, 5);
                    } else if (AnimFrame == 22) {
                        if (AttackCombo != 1) {
                            AnimDone = true;
                        }
                    } else if (AnimFrame == 23)
                        PlayAnimation(TimAttack[6], MyAnimations.ATTACK, 6);
                    else if (AnimFrame == 25)
                        PlayAnimation(TimAttack[2], MyAnimations.ATTACK, 2);
                    else if (AnimFrame == 26)
                        PlayAnimation(TimAttack[0], MyAnimations.ATTACK, 0);
                    else if (AnimFrame == 27) {
                        PlayAnimation(TimAttack[7], MyAnimations.ATTACK, 7);
                        AttackPart.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.05f), Quaternion.Euler(new Vector3(75, Turn, 0))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.05f), Quaternion.Euler(new Vector3(75, Turn, 315))), 0.1f);
                        AttackSprite.sortingOrder = 1;
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.05f), Quaternion.Euler(new Vector3(75, Turn, 270))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.05f), Quaternion.Euler(new Vector3(75, Turn, 225))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.05f), Quaternion.Euler(new Vector3(75, Turn, 180))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.05f), Quaternion.Euler(new Vector3(75, Turn, 135))), 0.1f);
                        Damaged.Clear();
                        GameManager.Swing2.Play();
                        if (TurnedLeft == true)
                            CurrentAttack = Instantiate(AttackHitBox, new Vector2(transform.position.x - 0.6f, transform.position.y), Quaternion.identity) as GameObject;
                        if (TurnedRight == true)
                            CurrentAttack = Instantiate(AttackHitBox, new Vector2(transform.position.x + 0.6f, transform.position.y), Quaternion.identity) as GameObject;
                        CurrentAttack.transform.parent = transform;
                        Destroy(CurrentAttack, 0.1f);
                        if (OnAir == false) {
                            PlayerBody.velocity = new Vector2(0,  PlayerBody.velocity.y);
                        }
                    } else if (AnimFrame == 34) {
                        if (AttackCombo != 2 || OnAir == true) {
                            AnimDone = true;
                        }
                    } else if (AnimFrame == 35)
                        PlayAnimation(TimAttack[13], MyAnimations.ATTACK, 13);
                    else if (AnimFrame == 37)
                        PlayAnimation(TimAttack[19], MyAnimations.ATTACK, 19);
                    else if (AnimFrame == 39)
                        PlayAnimation(TimAttack[12], MyAnimations.ATTACK, 12);
                    else if (AnimFrame == 49) {
                        if (OnAir == false) {
                            if (TurnedLeft)
                                PlayerBody.velocity = new Vector2(-10f,  PlayerBody.velocity.y);
                            if (TurnedRight)
                                PlayerBody.velocity = new Vector2(10f,  PlayerBody.velocity.y);
                        }
                        PlayAnimation(TimAttack[9], MyAnimations.ATTACK, 9);
                    } else if (AnimFrame == 50)
                        PlayAnimation(TimAttack[14], MyAnimations.ATTACK, 14);
                    else if (AnimFrame == 51) {
                        PlayAnimation(TimAttack[15], MyAnimations.ATTACK, 15);
                        AttackSprite.sortingOrder = 1;
                        AttackPart.transform.localScale = new Vector3(1.2f, 1.2f, 1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(20, Turn, 35.5f))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(20, Turn, -10))), 0.1f);
                        Destroy(Instantiate(AttackPart, new Vector2(transform.position.x, transform.position.y - 0.2f), Quaternion.Euler(new Vector3(20, Turn, 305))), 0.1f);
                        if (TurnedLeft == true)
                            Destroy(Instantiate(Smoke, new Vector2(Sword.transform.position.x - 1f, Sword.transform.position.y - 0.2f), Quaternion.Euler(new Vector3(260, 0, 0))), 1f);
                        if (TurnedRight == true)
                            Destroy(Instantiate(Smoke, new Vector2(Sword.transform.position.x + 1f, Sword.transform.position.y - 0.2f), Quaternion.Euler(new Vector3(260, 0, 0))), 1f);
                        Damaged.Clear();
                        GameManager.Ground.Play();
                        if (TurnedLeft == true)
                            CurrentAttack = Instantiate(AttackHitBox, new Vector2(transform.position.x - 0.6f, transform.position.y), Quaternion.identity) as GameObject;
                        if (TurnedRight == true)
                            CurrentAttack = Instantiate(AttackHitBox, new Vector2(transform.position.x + 0.6f, transform.position.y), Quaternion.identity) as GameObject;
                        CurrentAttack.transform.parent = transform;
                        Destroy(CurrentAttack, 0.1f);
                        if (OnAir == false)
                            PlayerBody.velocity = new Vector2(0,  PlayerBody.velocity.y);
                        StopMove = true;
                    }
                }
                #endregion
                #region Climb
                if (CurrentAnimation == MyAnimations.CLIMB) {
                    AnimPause = 2f;
                    if ((Input.GetKey(KeyLeft) || Input.GetKey(KeyRight)) && !Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow)) {
                        AnimDone = true;
                    }
                    if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.UpArrow)) {
                        PlayerBody.velocity = new Vector2(0, -3);
                        if (PauseTimer >= AnimPause) {
                            AnimTimer = AnimTimer > TimClimb.Length - 2 ? 0 : AnimTimer + 1;
                            PauseTimer = 0;
                        }
                        PlayAnimation(TimClimb[AnimTimer], MyAnimations.CLIMB, AnimTimer);
                    }
                    if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow)) {
                        PlayerBody.velocity = new Vector2(0, 3);
                        if (PauseTimer >= AnimPause) {
                            AnimTimer = AnimTimer == 0 ? TimClimb.Length - 1 : AnimTimer - 1;
                            PauseTimer = 0;
                        }
                        PlayAnimation(TimClimb[AnimTimer], MyAnimations.CLIMB, AnimTimer);
                    }
                    if (!Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.DownArrow)) {
                        PlayerBody.velocity = new Vector2(0, 0);
                    }
                }
                #endregion
                #region Block
                if (CurrentAnimation == MyAnimations.BLOCK) {
                    if (Input.GetKey(KeyBlock))
                        AnimDuration++;
                    BlockTimer = BlockTimer > TimBlock.Length - 2 ? 0 : BlockTimer + 1;
                    PlayAnimation(TimBlock[BlockTimer], MyAnimations.BLOCK, BlockTimer);
                    CurrentShield.transform.position = transform.position;
                    //CurrentShield.transform.localScale -= new Vector3(0.0025f, 0.0025f, 0);
                    //if (CurrentShield.transform.localScale.x < 0) AnimDone = true;
                }
                #endregion
                if (AnimCountdown == true) {
                    AnimFrame++;
                    if (AnimFrame >= AnimDuration)
                        AnimDone = true;
                }
            }
            if (AnimDone == true || Disabled == true) {
                if (CurrentAnimation == MyAnimations.DASHATTACK) {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 0.3f);
                    GetComponent<BoxCollider2D>().enabled = true;
                    GetComponent<CircleCollider2D>().offset = new Vector2(0, -0.3f);
                    transform.FindChild("Collider").GetComponent<BoxCollider2D>().offset = new Vector2(0, -0.5f);
                    FallAnimation = true;
                    EndingAnimation = false;
                    Sword.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
                    JumpDashDuration = 0;
                    PreDashDuration = 0;
                    StartDone = false;
                    Sword.GetComponent<BoxCollider2D>().enabled = false;
                }
                if (CurrentShield != null)
                    Destroy(CurrentShield);
                HandSprite.enabled = true;
                PlayerBody.gravityScale = 1f;
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
                CurrentAnimation = MyAnimations.NOTHING;
                AnimDone = false;
                PlayerAnim = false;
                AnimFrame = 0;
                Cooldown = 2;
                DashCooldown = 20;
                Reset();
                StopMove = false;
                Damaged.Clear();
            }
        }
        if (Disabled == true) {
            if (DisabledStart == false) {
                AnimTimer = 0;
                AnimPause = 1f;
                DisabledStart = true;
            }
            if (AnimTimer < TimPreDamaged.Length - 1&& PreDamagedDone == false) {
                if (PauseTimer >= AnimPause) {
                    AnimTimer++;
                    PauseTimer = 0;
                }
                PlayAnimation(TimPreDamaged[AnimTimer], MyAnimations.PREDAMAGED, AnimTimer);
            } else {
                if (AnimTimer >= TimDamaged.Length) {
                    AnimTimer = 0;
                }
                PreDamagedDone = true;
                if (PauseTimer >= AnimPause) {
                    AnimTimer = AnimTimer > TimDamaged.Length - 2 ? 0 : AnimTimer + 1;
                    PauseTimer = 0;
                }
                PlayAnimation(TimDamaged[AnimTimer], MyAnimations.DAMAGED, AnimTimer);
            }
        }
        if (PlayerAnim == false && Disabled == false) {
            if (OnGround == true) {
                if (Idle == true) {
                    if (PauseTimer >= AnimPause) {
                        AnimTimer = AnimTimer > TimIdle.Length - 2 ? 0 : AnimTimer + 1;
                        PauseTimer = 0;
                        if (AnimTimer < TimIdle.Length * 1 / 4 || AnimTimer > TimIdle.Length * 3 / 4) {
                            Position.y -= AnimTimer / 5000f;
                            Sword.transform.Rotate(0, 0, -AnimTimer / 100f);
                        }
                        if (AnimTimer > TimIdle.Length * 1 / 4 && AnimTimer < TimIdle.Length * 3 / 4) {
                            Position.y += AnimTimer / 5000f;
                            Sword.transform.Rotate(0, 0, AnimTimer / 100f);
                        }
                        Sword.transform.localPosition = Position;
                    }
                    TimSprite.sprite = TimIdle[AnimTimer];
                }
                if (Moving == true) {
                    if (PauseTimer >= AnimPause) {
                        AnimTimer = AnimTimer > TimMovement.Length - 2 ? 0 : AnimTimer + 1;
                        PauseTimer = 0;
                    }
                    PlayAnimation(TimMovement[AnimTimer], MyAnimations.MOVEMENT, AnimTimer);
                }
            }
            if (OnAir == true) {
                AnimPause = 1f;
                if (PauseTimer >= AnimPause) {
                    PauseTimer = 0;
                    if (PlayerBody.velocity.y > 0) {
                        AnimTimer = AnimTimer > TimJumpUp.Length - 2 ? 0 : AnimTimer + 1;
                        PlayAnimation(TimJumpUp[AnimTimer], MyAnimations.JUMPUP, AnimTimer);
                    }
                    if (PlayerBody.velocity.y < 0) {
                        AnimTimer = AnimTimer > TimJumpDown.Length - 2 ? 0 : AnimTimer + 1;
                        PlayAnimation(TimJumpDown[AnimTimer], MyAnimations.JUMPDOWN, AnimTimer);
                    }
                }
            }
        }
    }
    void PlayAnimation(Sprite sprite, MyAnimations Animation, int n) {
        TimSprite.sprite = sprite;
        SwordAnimation(Animation, n);
    }
    void SwordAnimation(MyAnimations Animation, int n) {
        if (Animation == MyAnimations.MOVEMENT) {
            if (n == 0)
                SetSword(-0.2f, -0.26f, 300f);
            else if (n == 1)
                SetSword(-0.1f, -0.25f, 330f);
            else if (n == 2)
                SetSword(-0.1f, -0.24f, 330f);
            else if (n == 3)
                SetSword(-0.05f, -0.22f, 330f);
            else if (n == 4)
                SetSword(0.1f, -0.22f, 340f);
            else if (n == 5)
                SetSword(0.2f, -0.17f, 0f);
            else if (n == 6)
                SetSword(0.18f, -0.16f, 0f);
            else if (n == 7)
                SetSword(0.1f, -0.16f, 0f);
            else if (n == 8)
                SetSword(0f, -0.18f, 350f);
            else if (n == 9)
                SetSword(-0.05f, -0.2f, 345f);
            else if (n == 10)
                SetSword(-0.1f, -0.22f, 335f);
            else if (n == 11)
                SetSword(-0.2f, -0.27f, 310f);
            else if (n == 12)
                SetSword(-0.25f, -0.27f, 300f);
            else if (n == 13)
                SetSword(-0.4f, -0.27f, 270f);
            else if (n == 14)
                SetSword(-0.4f, -0.22f, 260f);
            else if (n == 15)
                SetSword(-0.42f, -0.21f, 245f);
            else if (n == 16)
                SetSword(-0.42f, -0.18f, 245f);
            else if (n == 17)
                SetSword(-0.45f, -0.12f, 215f);
            else if (n == 18)
                SetSword(-0.5f, -0.1f, 215f);
            else if (n == 19)
                SetSword(-0.47f, -0.12f, 230f);
            else if (n == 20)
                SetSword(-0.45f, -0.13f, 230f);
            else if (n == 21)
                SetSword(-0.45f, -0.15f, 240f);
            else if (n == 22)
                SetSword(-0.4f, -0.25f, 250f);
            else if (n == 23)
                SetSword(-0.38f, -0.28f, 250f);
            else if (n == 24)
                SetSword(-0.36f, -0.34f, 250f);
            else if (n == 25)
                SetSword(-0.26f, -0.32f, 270f);
            else if (n == 26)
                SetSword(-0.2f, -0.28f, 300f);
        } else if (Animation == MyAnimations.PREDASH) {
            if (n == 0)
                SetSword(-0.25f, -0.05f, 250f);
            else if (n == 1)
                SetSword(-0.12f, -0.1f, 260f);
            else if (n == 2)
                SetSword(-0.05f, -0.05f, 270f);
            else if (n == 3)
                SetSword(0.1f, 0f, 280f);
            else if (n == 4)
                SetSword(0.2f, 0.08f, 275f);
        } else if (Animation == MyAnimations.JUMPUP) {
            SetSword(-0.41f, -0.18f, 240f);
        } else if (Animation == MyAnimations.JUMPDOWN) {
            SetSword(-0.43f, -0.14f, 260f);
        } else if (Animation == MyAnimations.DASHATTACK) {
            if (n == 0)
                SetSword(0.3f, 0f, Sword.transform.localEulerAngles.z);
            else if (n == 1)
                SetSword(0.23f, -0.07f, 100f);
        } else if (Animation == MyAnimations.ATTACK) {
            if (n == 0) {
                SetSword(-0.02f, -0.28f, 290f, 230f, 130f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 1) {
                SetSword(0.15f, -0.27f, 290f, 200f, 100f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 2) {
                SetSword(0.15f, -0.25f, 284f, 212f, 10f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 3) {
                SetSword(0.28f, -0.2f, 286f, 170f, 11f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = -1;
            } else if (n == 4) {
                SetSword(0.28f, -0.2f, 290f, 165f, 312f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = -1;
            } else if (n == 5) {
                SetSword(0.2f, -0.2f, 290f, 164f, 300f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = -1;
            } else if (n == 6) {
                SetSword(0.2f, -0.28f, 300f, 143f, 330f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = -1;
            } else if (n == 7) {
                SetSword(-0.6f, -0.05f, 293f, 156f, 270f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 8) {
                SetSword(0.25f, 0.03f, 353f, 30f, 307f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 9) {
                SetSword(0.32f, 0.23f, 12f, 351f, 341f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 10) {
                SetSword(0.25f, 0.3f, 0f, 0f, 0f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 11) {
                SetSword(0.22f, 0.33f, 3f, 11f, 8f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 12) {
                SetSword(-0.18f, 0.26f, 3f, 11f, 28f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 13) {
                SetSword(-0.3f, 0.02f, 0f, 0f, 90f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 14) {
                SetSword(0.5f, -0.13f, 0f, 0f, 330f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 15) {
                SetSword(0.5f, -0.3f, 0f, 0f, 260f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            } else if (n == 19) {
                SetSword(-0.24f, 0.17f, 0f, 0f, 60f);
                SwordSprite.sortingOrder = HandSprite.sortingOrder = 1;
            }
        } else if (Animation == MyAnimations.BLOCK) {
            SetSword(0.06f, -0.2f, 335f);
        } else if (Animation == MyAnimations.PREDAMAGED) {
            if (n == 0) {
                SetSword(-0.31f, -0.27f, 0f);
            } else if (n == 1) {
                SetSword(-0.1f, -0.2f, 15f);
            } else if (n == 2) {
                SetSword(-0.04f, -0.17f, 25f);
            } else if (n == 3) {
                SetSword(-0.05f, -0.2f, 30f);
            } else if (n == 4) {
                SetSword(0.08f, -0.13f, 40f);
            } else if (n == 5) {
                SetSword(0.06f, -0.08f, 60f);
            }
        } else if (Animation == MyAnimations.DAMAGED) {
            SetSword(-0.02f, -0.2f, 80f);
        } else if (Animation == MyAnimations.CLIMB) {
            if (n == 0) {
                SetSword(-0.265f, 0.1f, 215f);
            } else if (n == 1) {
                SetSword(-0.265f, 0.13f, 210f);
            } else if (n == 2) {
                SetSword(-0.265f, 0.1f, 215f);
            } else if (n == 3) {
                SetSword(-0.265f, 0.07f, 225f);
            } else if (n == 4) {
                SetSword(-0.265f, 0.05f, 230f);
            } else if (n == 5) {
                SetSword(-0.265f, 0.05f, 230f);
            } else if (n == 6) {
                SetSword(-0.265f, 0.07f, 225f);
            } else if (n == 7) {
                SetSword(-0.265f, 0.07f, 225f);
            }
        }
    }
    void SetSword(float PosX, float PosY, float RotZ) {
        SetSword(PosX, PosY, 0, 0, RotZ);
    }
    void SetSword(float PosX, float PosY, float RotX, float RotY, float RotZ) {
        Sword.transform.localPosition = new Vector2(PosX, PosY);
        Sword.transform.localEulerAngles = new Vector3(RotX, RotY, RotZ);
    }
    void Reset() {
        Move = 0;
        AnimTimer = 0;
        Timer = 0;
        transform.Rotate(0, 0, -transform.localEulerAngles.z);
        if (OnAir == false) {
            Position = SwordStartingPosition;
            Sword.transform.localEulerAngles = SwordStartingRotation;
            Sword.transform.localPosition = SwordStartingPosition;
        }
    }
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Potion") {
            GetComponent<Health>().PlayerHealth += 15;
            if (GetComponent<Health>().PlayerHealth > GetComponent<Health>().PlayerMaxHealth) {
                GetComponent<Health>().PlayerHealth = GetComponent<Health>().PlayerMaxHealth;
            }
            GameManager.ShowDamage(transform.position, 15, true);
            GameManager.Eat.Play();
            Destroy(other.gameObject);
        }
    }
    void OnTriggerStay2D(Collider2D other) {
        if (other.tag == "Stige") {
            if (Input.GetKeyDown(KeyCode.UpArrow) && Disabled == false && PlayerAnim == false) {
                transform.position = new Vector2(other.transform.position.x, transform.position.y);
                PlayerBody.velocity = new Vector2(0, 0);
                AnimTimer = 0;
                FrameAnimation(MyAnimations.CLIMB);
                PlayerBody.gravityScale = 0f;
                HandSprite.enabled = false;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) && Disabled == false && PlayerAnim == false && OnGround == true) {
                transform.position = new Vector2(other.transform.position.x, transform.position.y);
                PlayerBody.velocity = new Vector2(0, 0);
                AnimTimer = 0;
                FrameAnimation(MyAnimations.CLIMB);
                PlayerBody.gravityScale = 0f;
                HandSprite.enabled = false;
            }
        }
    }
    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Stige") {
            if (CurrentAnimation == MyAnimations.CLIMB) {
                AnimDone = true;
            }
        }
    }
}

