﻿using UnityEngine;
using System.Collections;

public class FloorDetector : MonoBehaviour {
    GameObject Player;
    bool Done;
    void Start()
    {
        if (Done == false)
        {
            Player = transform.parent.gameObject;
            Done = true;
            Player.GetComponent<Movement>().FloorGround = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (Done == false)
        {
            Player = transform.parent.gameObject;
            Done = true;
        }
        if (other.tag == "Floor")
        {
            Player.GetComponent<Movement>().FloorAir = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (Done == false)
        {
            Player = transform.parent.gameObject;
            Done = true;
        }
        if (other.tag == "Floor")
        {
            transform.parent.gameObject.GetComponent<Movement>().FloorGround = true;
        }
    }
}
