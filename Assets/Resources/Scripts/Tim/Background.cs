﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {
    public GameObject Player;
    public GameObject BackgroundWall;
    Sprite[] BackgroundAnim;
    SpriteRenderer MySprite;
    float Pause = 5;
    float Timer;
    int BackgroundNum;
	// Use this for initialization
	void Start () {
        MySprite = GetComponent<SpriteRenderer>();
	    BackgroundAnim = Resources.LoadAll<Sprite>("Sprites/Background");
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = BackgroundWall.transform.position = new Vector2(-Player.transform.position.x/10f, 0);
        transform.localPosition = new Vector2(transform.localPosition.x, -0.95f);
	}

    void FixedUpdate()
    {
        Timer++;
        if (Timer >= Pause)
        {
            Timer = 0;
            BackgroundNum = BackgroundNum > BackgroundAnim.Length - 2 ? 0 : BackgroundNum + 1;
            MySprite.sprite = BackgroundAnim[BackgroundNum];
        }
    }
}
