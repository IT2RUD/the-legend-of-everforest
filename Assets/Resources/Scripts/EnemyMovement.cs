﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    public GameObject Player;
    public GameObject Fireball;

    public float Speed;
    public float CoolDown;

    public bool IAmGomba;
    public bool IAmRabbit;

    SpriteRenderer EnemySprite;

    Sprite[] EnemyMoving;

    Rigidbody2D EnemyBody;

    float FireBallRange = 6;
    float AnimPause = 3f;
    float PauseTimer;
    float CurrentCoolDown;

    int AnimTimer;
    // Use this for initialization
    void Start()
    {
        if (IAmGomba == false && IAmRabbit == false)
        {
            Debug.Log("You forgot to add type to monster");
        }
        EnemySprite = GetComponent<SpriteRenderer>();
        EnemyMoving = Resources.LoadAll<Sprite>("Sprites/Enemies/Gomba/Moving");
        EnemyBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void FixedUpdate()
    {
        if (CurrentCoolDown > 0)
        {
            CurrentCoolDown--;
        }
        DoAnimation();
        if (Player.transform.position.x < transform.position.x + FireBallRange && Player.transform.position.x > transform.position.x - FireBallRange && CurrentCoolDown == 0)
        {
            Attack();
            return;
        }
        Move();
    }
    void DoAnimation()
    {
        PauseTimer++;
        if (PauseTimer >= AnimPause)
        {
            AnimTimer = AnimTimer > EnemyMoving.Length - 2 ? 0 : AnimTimer + 1;
            PauseTimer = 0;
            EnemySprite.sprite = EnemyMoving[AnimTimer];
        }
        if (Player.transform.position.x < transform.position.x)
        {
            EnemyBody.transform.localEulerAngles = new Vector3(0, 180, 0);
        }
        if (Player.transform.position.x > transform.position.x)
        {
            EnemyBody.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }
    void Attack()
    {
        if (IAmGomba == true)
        {
            GameObject Fire = Instantiate(Fireball, transform.position, Quaternion.identity) as GameObject;
            Fire.GetComponent<Rigidbody2D>().AddForce(new Vector2(Player.transform.position.x - transform.position.x, 0).normalized * 30f);
            CurrentCoolDown = CoolDown;
        }
    }
    void Move()
    {
        EnemyBody.velocity = new Vector2(Mathf.Sign(Player.transform.position.x - transform.position.x) * Speed, EnemyBody.velocity.y);
    }
}
